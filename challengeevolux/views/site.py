# _*_ encoding: utf8 _*_
from string import strip
from flask import Blueprint, render_template, \
    redirect, url_for, request, flash
from challengeevolux.database import db_session
from challengeevolux.models import Student, Challenge, ChallengeDone

blueprint = Blueprint('site', __name__,
                      template_folder='templates')


@blueprint.route('/')
def index():
    template_params = {'students': Student.query.all()}

    return render_template('/site/index.html', **template_params)


@blueprint.route('/pick_challenge/<student_id>')
def pick_challenge(student_id):
    student = Student.query.get(student_id)

    if not student:
        flash(u'Usuário inválido. Tem certeza que entrou com a url correta?', 'danger')
        return redirect(url_for('site.index'))

    next_challenge = Challenge.get_next_available(student.id)

    if next_challenge:
        return redirect(url_for('site.challenge', challenge_id=next_challenge.id, student_id=student_id))
    else:
        return render_template('/site/challenge_end.html')


@blueprint.route('/challenge/stu_<student_id>/no_<challenge_id>')
def challenge(student_id, challenge_id):
    student = Student.query.get(student_id)
    current_challenge = Challenge.query.get(challenge_id)

    if not student:
        flash(u'Usuário inválido. Tem certeza que entrou com a url correta?', 'danger')
        return redirect(url_for('site.index'))

    if not current_challenge:
        flash(u'Desafio inválido. Tem certeza que entrou com a url correta?', 'danger')
        return redirect(url_for('site.index'))

    if current_challenge.is_done(student.id):
        flash(u'Você já respondeu a esse exercício', 'warning')
        return redirect(url_for('site.index'))

    other_challenges = Challenge.get_available(student.id, current_challenge.id)

    template_params = {'student': student,
                       'challenge': current_challenge,
                       'challenges': other_challenges}

    return render_template('/site/challenge.html', **template_params)


@blueprint.route('/challenge/send', methods=['post'])
def challenge_send():
    student = Student.query.get(request.form['student_id'])
    current_challenge = Challenge.query.get(request.form['challenge_id'])

    if not student:
        flash(u'Usuário inválido. Tem certeza que entrou com a url correta?', 'danger')
        return redirect(url_for('site.index'))

    if not current_challenge:
        flash(u'Desafio inválido. Tem certeza que entrou com a url correta?', 'danger')
        return redirect(url_for('site.index'))

    if current_challenge.is_done(student.id):
        flash(u'Você já respondeu a esse exercício', 'warning')
        return redirect(url_for('site.index'))

    answer = strip(request.form['answer']) == current_challenge.result
    challenge_done_new = ChallengeDone(student.id, current_challenge.id, answer)

    db_session.add(challenge_done_new)
    db_session.commit()

    return redirect(url_for('site.pick_challenge', student_id=student.id))


@blueprint.route('/<path:path>')
def catch_all(path):
    flash(u"Tentasse acessar um recurso indisponível, "
          u"mas esquenta não: te trouxe de volta. :)", 'info')

    return redirect(url_for('site.index'))