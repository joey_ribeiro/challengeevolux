# _*_ encoding: utf8 _*_

from flask import Blueprint, \
    render_template, redirect, url_for, request, flash
from sqlalchemy.exc import IntegrityError
from challengeevolux.database import db_session
from challengeevolux.models import Student
from challengeevolux.forms import StudentForm

blueprint = Blueprint('admin.students', __name__,
                      url_prefix='/admin/students',
                      template_folder='templates')


@blueprint.route('/')
def index():
    return redirect(url_for('admin.students.list_'))


@blueprint.route('/list')
def list_():
    student_list = Student.query.all()
    template_params = {'students_list': student_list}

    return render_template('/admin/student_list.html', **template_params)


@blueprint.route('/edit/<student_id>', methods=['GET'])
def edit(student_id):
    student = Student.query.get(student_id)

    if not student:
        flash(u'Aluno não existe', 'warning')
        return redirect(url_for('admin.students.list_'))

    students_form = StudentForm(request.form, student)

    template_params = {'form': students_form}

    return render_template('/admin/student_form.html', **template_params)


@blueprint.route('/new', methods=['GET'])
def new():
    if request.method == 'GET':
        students_form = StudentForm()
        template_params = {'form': students_form}

        return render_template('/admin/student_form.html', **template_params)


@blueprint.route('/save', methods=['POST'])
def save():
    is_update = True if request.form['id'] else False

    if is_update:
        student = Student.query.get(request.form['id'])

        if not student:
            flash(u'Aluno não existe', 'warning')
            return redirect(url_for('admin.students.list_'))
    else:
        student = Student()

    students_form = StudentForm(request.form, student)

    if not students_form.validate():
        flash(u'Preencha os campos corretamente antes de prosseguir', 'warning')
        return redirect(url_for('admin.students.new'))
    else:

        student.name = request.form['name']
        student.email = request.form['email']

        try:
            if not is_update:
                db_session.add(student)

            db_session.commit()
        except IntegrityError:
            flash(u'Você já efetuou um cadastro com estas credenciais', 'warning')
            return redirect(url_for('admin.students.new'))
        except:
            flash(u'Ocorreu um erro interno. Tente novamente em instantes', 'warning')
            if is_update:
                return redirect(url_for('admin.students.list_'))
            else:
                return redirect(url_for('admin.students.new'))

        flash(u'Registro salvo com sucesso', 'success')
        return redirect(url_for('admin.students.list_'))


@blueprint.route('/del/<student_id>')
def del_(student_id):
    student = Student.query.get(student_id)

    if not student:
        flash(u'Aluno não existe', 'warning')
        return redirect(url_for('admin.students.list_'))

    db_session.delete(student)
    db_session.commit()

    flash(u'Registro excluído com sucesso', 'success')
    return redirect(url_for('admin.students.list_'))