# _*_ encoding: utf8 _*_

from flask import Blueprint, render_template

blueprint = Blueprint('admin', __name__,
                     url_prefix='/admin',
                     template_folder='templates')

# Default Route
@blueprint.route('/')
def index():
    template_params = {'current_menu': 'index'}

    return render_template('/admin/index.html', **template_params)
