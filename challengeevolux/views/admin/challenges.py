# _*_ encoding: utf8 _*_

from flask import Blueprint, \
    render_template, redirect, url_for, request, flash
from sqlalchemy.exc import IntegrityError
from challengeevolux.database import db_session
from challengeevolux.models import Challenge
from challengeevolux.forms import ChallengeAdminForm

blueprint = Blueprint('admin.challenges', __name__,
                      url_prefix='/admin/challenges',
                      template_folder='templates')


@blueprint.route('/')
def index():
    return redirect(url_for('admin.challenges.list_'))


@blueprint.route('/list')
def list_():
    challenge_list = Challenge.query.all()
    template_params = {'current_menu': 'challenges',
                       'challenges_list': challenge_list}

    return render_template('/admin/challenge_list.html', **template_params)


@blueprint.route('/edit/<challenge_id>', methods=['GET'])
def edit(challenge_id):
    challenge = Challenge.query.get(challenge_id)

    if not challenge:
        flash(u'Desafio não existe', 'warning')
        return redirect(url_for('admin.challenges.list_'))

    challenges_form = ChallengeAdminForm(request.form, challenge)

    template_params = {'current_menu': 'challenges',
                       'form': challenges_form}

    return render_template('/admin/challenge_form.html', **template_params)


@blueprint.route('/new', methods=['GET'])
def new():
    if request.method == 'GET':
        challenges_form = ChallengeAdminForm()
        template_params = {'current_menu': 'challenges',
                           'form': challenges_form}

        return render_template('/admin/challenge_form.html', **template_params)


@blueprint.route('/save', methods=['POST'])
def save():
    is_update = True if request.form['id'] else False

    if is_update:
        challenge = Challenge.query.get(request.form['id'])

        if not challenge:
            flash(u'Desafio não existe', 'warning')
            return redirect(url_for('admin.challenges.list_'))
    else:
        challenge = Challenge()

    challenges_form = ChallengeAdminForm(request.form, challenge)

    if not challenges_form.validate():
        flash(u'Preencha os campos corretamente antes de prosseguir', 'warning')
        return redirect(url_for('admin.challenges.new'))
    else:

        challenge.name = request.form['name']
        challenge.description = request.form['description']
        challenge.result = request.form['result']

        try:
            if not is_update:
                db_session.add(challenge)

            db_session.commit()
        except IntegrityError:
            flash(u'Você já efetuou um cadastro com estas credenciais', 'warning')
            return redirect(url_for('admin.challenges.new'))
        except:
            flash(u'Ocorreu um erro interno. Tente novamente em instantes', 'warning')
            if is_update:
                return redirect(url_for('admin.challenges.list_'))
            else:
                return redirect(url_for('admin.challenges.new'))

        flash(u'Registro salvo com sucesso', 'success')
        return redirect(url_for('admin.challenges.list_'))


@blueprint.route('/del/<challenge_id>')
def del_(challenge_id):
    challenge = Challenge.query.get(challenge_id)

    if not challenge:
        flash(u'Desafio não existe', 'warning')
        return redirect(url_for('admin.challenges.list_'))

    db_session.delete(challenge)
    db_session.commit()

    flash(u'Registro excluído com sucesso', 'success')
    return redirect(url_for('admin.challenges.list_'))