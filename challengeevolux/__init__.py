from flask import Flask
from challengeevolux.database import db_session
from views import admin
from views.admin import students, challenges
from views import site

app = Flask(__name__)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

app.secret_key = 'OpkoIuhYt67FYghi9u8Yij9u8y7t6fyHjnKLM'

# Admin panel
app.register_blueprint(admin.blueprint)
app.register_blueprint(students.blueprint)
app.register_blueprint(challenges.blueprint)

# Registering Site
app.register_blueprint(site.blueprint)