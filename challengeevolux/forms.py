# _*_ encoding: utf8 _*_

from wtforms import Form, TextField, TextAreaField, \
    HiddenField, validators


class StudentForm(Form):
    id = HiddenField()
    name = TextField(u'Nome', [validators.Length(min=4)])
    email = TextField(u'E-mail', [validators.Length(min=6)])


class ChallengeForm(Form):
    id = HiddenField()
    name = TextField(u'Nome', [validators.Length(min=4)])
    description = TextAreaField(u'Descrição', [validators.required()])


class ChallengeAdminForm(ChallengeForm):
    result = TextField(u'Resultado', [validators.required()])


class ChallengeSiteForm(ChallengeForm):
    student_id = HiddenField(validators=[validators.required()])
