from sqlalchemy import Column, Integer, String, Text, Boolean, ForeignKey, and_, func
from sqlalchemy.orm import relationship
from database import Base, db_session


class Student(Base):
    __tablename__ = 'student'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(120), unique=True)
    email = Column(String(120), unique=True)

    challenge_done = relationship("ChallengeDone",
                                  cascade="all, delete-orphan",
                                  passive_deletes=True)

    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email


class Challenge(Base):
    __tablename__ = 'challenge'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    description = Column(Text, nullable=False)
    result = Column(String(50), nullable=False)

    def __init__(self, name=None, description=None, result=None):
        self.name = name
        self.description = description
        self.result = result

    # Checks if student has finished this Challenge
    def is_done(self, student_id):
        is_current_challenge_done = db_session.query(func.count(ChallengeDone.id)). \
            filter(and_(ChallengeDone.challenge_id == self.id,
                        ChallengeDone.student_id == student_id)).all()

        return True if is_current_challenge_done[0][0] >= 1 else False

    @staticmethod
    def get_next_available(student_id):
        query = Challenge.query.\
            filter(~Challenge.id.in_(
                db_session.query(ChallengeDone.challenge_id).filter(ChallengeDone.student_id == student_id)))

        return query.first()

    @staticmethod
    def get_available(student_id, current_challenge_id):
        query = Challenge.query.\
            filter(and_(~Challenge.id.in_(
                db_session.query(ChallengeDone.challenge_id).filter(ChallengeDone.student_id == student_id)),
                Challenge.id != current_challenge_id))

        return query.all()

class ChallengeDone(Base):
    __tablename__ = 'challenge_done'
    id = Column(Integer, primary_key=True, autoincrement=True)
    student_id = Column(Integer, ForeignKey('student.id'))
    challenge_id = Column(Integer, ForeignKey('challenge.id'))
    result_ok = Column(Boolean, default=False)

    def __init__(self, student_id, challenge_id, result_ok):
        self.student_id = student_id
        self.challenge_id = challenge_id
        self.result_ok = result_ok
